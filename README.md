WAR!

What is war?
	War is a super basic card 'game' that requires no skill at all. Infact other then the original shuffling of the cards there is no randomness whatsoever.
	This means that the entire game is predictable from the exact start, which makes it quite boring as a card game.
	This saves you the trouble of ever having to actually play out a game of war ever again!

How to install and run program:
	Clone the repository, compile the code, run the executable.

Rules:
	Generally you start out by dealing out part of the deck to all players. In this program we are going to start after that. You simply will input what got delt to whom, and in what order.

	The following steps if followed exactly will play a game of War, just like thsi program will.

	It also is nessicary that if at any time any player is out of cards in their deck, and then needs to take another card off the top they are instantly removed from the game.
	Their cards will be collected as per normal during the next cleanup.

	Gameplay:
		1. Each player reveals the top card of their deck, and places it infront of them.
		2. Determine what the highest card that has just been revealed is, whoever does not have that card is 'out' for the rest of the round.
		3. If there is only one player left in this round then go to the rules for starting the next round.
		4. Each player that is left in this round puts down the top card of their deck facedown, behind the last revealed card.
		5. Each player that is left in this round puts an additonal card from the top of thier deck facedown on top of the last one.
		6. Each player that is left in now reveals a card, placing it behind the two facedown cards.
		7. If there are no more players left in the game, then the game is a draw between the last players who were in the game.
		8. If there is only one player left in the game that player is the winner of the game and the game is over.
		9. Go back to step 2.

	Starting next round:
		1. Who ever is left in this round is this rounds "winner"
		2. We are going to refer to a stack of cards called the "cleanup pile", it currently is empty.
		3. Follow the instructions for cleanup for each player who has put cards on the table this round, starting with the winner and going right.
		4. Put the winner's deck on top of the ceanup.
		5. The cleanup pile is now the winner's deck.
		6. Declare the next round started, meaning everyone who was out for just the round is now in.
		7. Go back to instructions for gameplay and start from instruction 1.

	Cleanup:
		1. Starting with the first card this player put down that you have not moved to the cleanup pile, put it on the cleanup pile facedown.
		2. If there are no more cards left that this player put down, this player is complete with cleanup.
		3. Next there should be a stack of 2 cards facedown, take these and put them on the cleanup pile, maintaining their order.
				In the case that there is only one card because this player ran out of cards, then put that card on the cleanup pile, and the cleanup is done for this player.
		4. If there is no more cards this player has put down, this player is complete with cleanup.
		5. Go back to step 1.


Input:
	It takes in a JSON formated file, containing an array of arrays of strings.
	Each string represents a standard playing card, represented by just it's number.
	The suit is ignored in this game so we don't need a suit inputed.
		Number: (numbers are ordered from least to greatest here)
			'2' => 2
			'3' => 3
			'4' => 4
			'5' => 5
			'6' => 6
			'7' => 7
			'8' => 8
			'9' => 9
			'0' => 10
			'j' or 'J' => Jack
			'q' or 'Q' => Queen
			'k' or 'K' => King
			'a' or 'A' => Ace

	Each inner array in the JSON file is what a player holds in order, with the bottom card being the last one in the array.
	The outter array is the pool of players. The nth player is sitting to the right of the nth + 1 player, with the exception of the last entry in the array which is sitting to the right of the 0th player (it's a circle).

Output:
	The output of the game, which could be:
		infinite loop, if the game never ends.
		a single winner if one person wins.
		the set of winners if there is a draw.